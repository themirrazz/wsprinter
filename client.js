/**
 * Licensed under GNU General Public License 3.0
 * (c) themirrazz 2022
 */


class PrinterClient {
    #printjob=0;
    #socket;
    #hostname="";
    constructor(hostname) {
        this.#hostname=hostname;
        this.#socket=io(hostname);
        this.#printjob=0;
    }
    getDetails() {
        return new Promise((resolve,reject)=>{
            this.#socket.emit("system.details",function(data,status){
                if(status===500) {
                    reject(data)
                } else {
                    resolve(data)
                }
            });
        });
    }
}

